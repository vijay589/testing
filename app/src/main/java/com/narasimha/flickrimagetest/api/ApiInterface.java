package com.narasimha.flickrimagetest.api;


import com.narasimha.flickrimagetest.models.FlickrResult;

import org.json.JSONObject;

import java.util.ArrayList;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("services/rest/?method=flickr.photos.search&api_key=3e7cc266ae2b0e0d78e279ce8e361736&format=json&nojsoncallback=1&safe_search=1")
    Call<FlickrResult> getImages(@Query("text") String text, @Query("page") String page);

}
