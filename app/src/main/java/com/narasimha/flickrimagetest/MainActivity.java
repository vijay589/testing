package com.narasimha.flickrimagetest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.narasimha.flickrimagetest.api.ApiClient;
import com.narasimha.flickrimagetest.api.ApiInterface;
import com.narasimha.flickrimagetest.databinding.ActivityMainBinding;
import com.narasimha.flickrimagetest.models.FlickrResult;
import com.narasimha.flickrimagetest.models.Photo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    ApiInterface apiInterface;

    List<Photo> photoList = new ArrayList<>();

    int offset=1;
    int itemCount = 100;
    boolean hasNextPage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        binding.rvSearchList.setLayoutManager(new GridLayoutManager(MainActivity.this, 3));
        binding.rvSearchList.setAdapter(new ImageAdapter());

        binding.ivSearch.setOnClickListener(v -> {
            if (binding.etSearchKey.getText().toString().isEmpty()){
                Toast.makeText(MainActivity.this, "Search word is Empty", Toast.LENGTH_SHORT).show();
            }
            else {
                offset = 1;
                hideKeyboard(MainActivity.this);
                getImages(binding.etSearchKey.getText().toString().trim());
            }
        });

    }

    private class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            return new ViewHolder(getLayoutInflater().inflate(R.layout.item_image, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Glide.with(MainActivity.this).load(photoList.get(position).getImageUrl()).placeholder(R.drawable.progress_animation)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.ivImage);

            if (position == (photoList.size() - 1)) {
                if (hasNextPage) {
                    offset = offset + 1;
                    getImages(binding.etSearchKey.getText().toString());
                }
            }

        }

        @Override
        public int getItemCount() {
            return photoList.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            ImageView ivImage;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                ivImage = itemView.findViewById(R.id.iv_image);
            }
        }
    }

    private void getImages(String keyword) {
        Call<FlickrResult> call = apiInterface.getImages(keyword, ""+offset);
        call.enqueue(new Callback<FlickrResult>() {
            @Override
            public void onResponse(@NonNull Call<FlickrResult> call, @NonNull Response<FlickrResult> response) {
                if (response.body() != null) {
                    FlickrResult result = response.body();
                    if (offset==1){
                        photoList.clear();
                    }
                    photoList.addAll(result.getPhotos().getPhoto());

                    if (photoList.size()%itemCount==0 && result.getPhotos().getPhoto().size()>0){
                        hasNextPage = true;
                    }else {
                        hasNextPage = false;
                    }

                    binding.rvSearchList.getAdapter().notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@NonNull Call<FlickrResult> call, @NonNull Throwable t) {

            }
        });
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}